function getPromise(url) {
    return fetch(url);
}

function useJSON(data) {
    let table = document.createElement('table');
    for (const guitar of data.Guitars.Guitar) {
        $(table).append(`<tr><td>${guitar.Brand}, model: ${guitar.Model}</tr></td>`)
    }
    document.body.appendChild(table);
}

$(() => {
   $('button').click(() => {
        $.getJSON('res/guitars.json', useJSON)
        getPromise("Events.js")
        .then((response) => {
            return response.text()
        })
        .then(data => {
            let dataDiv = document.createElement("div");
            dataDiv.innerText = data;
            document.body.appendChild(dataDiv)
        })
        .catch(error => { 
            console.error('Error fetching the file:', error); 
        }); 

   });
});