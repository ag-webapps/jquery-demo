function onblur(input, textField){
    $(textField).text($(input).val());
    $(textField).removeAttr("edit");
    $(textField).one('click', edit);

    $(input).remove();
}

function edit(){
    $('button').trigger('asdf');

    let input = document.createElement("input");
    input.setAttribute('id', 'edit');
    input.value = $(this).text();

    $(this).text("");

    this.appendChild(input);
    input.focus();

    $(input).on('blur', () => onblur(input, this));
}

$(function () {
    $('td').one('click', edit);
    $('button').bind('asdf', function () {
        alert('The cake is a lie!');
    });

    //$('*').unbind();
    $('button').unbind();
});