[![pipeline](https://gitlab.com/ag-webapps/jquery-demo/badges/main/pipeline.svg)](https://gitlab.com/ag-webapps/jquery-demo/-/commits/main)

# Collection Tracker

This project is a jQuery DOM manipulation demonstration, built using HTML, scss and JavaScript (jQuery).

## See for yourself

Bear in mind that all styling in this project is only for DOM manipulation purposes. The content is raw and doesn't look good, as looks were not part of this demo.

The demo is hosted online.
### [`View demo`](https://ag-webapps.gitlab.io/jquery-demo/)

## Tinkering guide

Clone the repository and run a `npm install` in it.

Once all dependencies are installed you can use scripts defined for this project:
- `npm run build` - to build the application into a static website (creates the `dist` directory with the resulting website)
- `npm run clean` - to clean up the project, removing the `dist` directory
- `npm run develop` - to build the project, serve it on localhost and run watch scripts that update the `dist` directory's contents as well as the page content in real time, whenever a source file changes